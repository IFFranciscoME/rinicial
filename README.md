# RInicial
Codigo basico inicial para a listar R y RStudio

Con esto podremos "estandarizar" el uso de R y RStudio a un nivel mínimo para facilitar la coolaboración entre 
todos los desarrolladores.

Algunos recursos útiles para esto.

- Descargar la última versión de R - [CRAN](https://cran.r-project.org/)
- Descargar la última versión de RStudio - [RStudio](https://www.rstudio.com/products/RStudio/)
- Descargar la última versión de GitHub Windows - [Windows GitHub](https://windows.github.com/)
- Descargar la última versión de GitHub MAC - [Mac GitHub](https://mac.github.com/)
- Manual/Intro Básicos de GitHub - [Conociendo GitHub](http://conociendogithub.readthedocs.org/en/latest/data/introduccion/)
- Página de Guías, oficial de GitHub - [Guides](https://guides.github.com/)
