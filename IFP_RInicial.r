# -- ----------------------------------------------------------------------------------------- -- #
# -- Desarrollador Inicial: FranciscoME ------------------------------------------------------ -- #
# -- GitHub https://github.com/IntrepidFundParty/RInicial ------------------------------------ -- #
# -- Licencia: GNU General Public License ---------------------------------------------------- -- #
# -- ----------------------------------------------------------------------------------------- -- #

# -- Paso 0 -- Estilo de programacion e identacion para R de Google -------------------------- -- #
# Seguir la guia de R de google, "Google's R Stye Guide" para escribir los codigos
# https://google-styleguide.googlecode.com/svn/trunk/Rguide.xml

# -- Paso 1 -- Configuracion de espacio para texto-codigo ------------------------------------ -- #

# Linea de comentarios: El ambiente de R o RStudio, en particular la parte de edicion de codigo,
# debe de estar configurado a un espacio horizontal de 105 columnas. Visualizar la regla vertical
# es una opcion util.

# -- Paso 2 -- Funciones iniciales en cada codigo -------------------------------------------- -- #

# Se deben utilizar las siguientes 4 instrucciones al inicio de cada codigo

closeAllConnections() # Cierra todas las conexiones hacia internet, archivos locales, etc.
rm(list=ls())         # Borra todos los objetos en el Environment
cat("\014")           # Limpia la consola
dev.off()             # Cierra el dispositivo de graficos, borra todas los plots generados

# -- Paso 3 -- Haber instalado los paquetes -------------------------------------------------- -- #

# Cargar paquetes y de no estar instalados los instala y procede a cargarlos dentro de la misma 
# instruccion. En caso de que este pedazo de codigo no funcione o no instale algunos paquetes, 
# habra que correrlo nuevamente y verificar los errores uno por uno. 

pkg <- c("base","downloader","fBasics","forecast","grid","gridExtra","ggplot2","httr",
         "jsonlite","lubridate","moments","PerformanceAnalytics","plyr","quantmod",
         "reshape2","RCurl","stats","scales","tseries","TTR","TSA","xts","xlsx","zoo")

inst <- pkg %in% installed.packages()
if(length(pkg[!inst]) > 0) install.packages(pkg[!inst])
instpackages <- lapply(pkg, library, character.only=TRUE)

# -- Paso 4 -- Formato para numeros, fechas y horas ------------------------------------------ -- #

# Instrucciones para, la primera, evitar que numeros muy grandes o muy pequeños sean expresados con
# notacion cientifica. La segunda para especificar el uso horario y otras configuraciones de tiempo
# y fecha con el formato local de la maquina donde se esta corriendo el codigo.

options("scipen"=100,"getSymbols.warning4.0"=FALSE,concordance=TRUE)
Sys.setlocale(category = "LC_ALL", locale = "")

# -- Paso 5 -- Conexion a internet ----------------------------------------------------------- -- #

# Verificar que se puede acceder y cargar el codigo en linea de los multiples repositorios utiles
# para el fondo. Se debede comprobar que todas las instrucciones estan contenidas en el environment

ROandaAPI <- "https://raw.githubusercontent.com/IFFranciscoME/ROandaAPI/master/ROandaAPI.R"
downloader::source_url(ROandaAPI,prompt=FALSE,quiet=TRUE)

